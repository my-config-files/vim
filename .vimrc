" Yann VOTE personal vimrc
"
" 2014-11-18, Yann VOTE
"
" Use Vim defaults instead of old Vi
set nocompatible

" Use pathogen to automatically install plugins
filetype off
call pathogen#infect()
call pathogen#helptags()

" Turn on syntax highlighting
filetype plugin indent on
syntax on

" Use /tmp folder for swapfiles
set directory=/tmp//

" Persistent undo
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000

" Use gruvbox color scheme
set background=dark
let g:gruvbox_italic=1
colorscheme gruvbox

" Set leader key to ,
let mapleader = ","
let g:mapleader = ","

" Activate mouse
set mouse=a

" Auto reload file when changed
set autoread

" Files to ignore when completing filenames
set wildignore+=*.pyc,*.so,*.swp,*.zip,*.tar.gz

" Increase maxmempattern for .rst
set mmp=6000

" Set Ctrl+Tab and Ctrl+Shift+Tab to switch between buffers
nmap <C-Tab> :bnext<CR>
nmap <C-S-Tab> :bprevious<CR>

" Highlight trailing spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Spell check some files
augroup spell_check
    autocmd!
    autocmd FileType gitcommit setlocal spell spelllang=en
    autocmd FileType markdown setlocal spell spelllang=en
    autocmd FileType rst setlocal spell spelllang=en
augroup END

" Grey characters past column 80 in Python files
augroup python_line_max_length
    autocmd!
    autocmd FileType python  setlocal cc=80
    autocmd FileType python set nowrap
augroup END

" Grey characters past column 100 in LaTeX files
augroup latex_line_max_length
    autocmd!
    autocmd FileType tex  setlocal cc=100
    autocmd FileType tex set nowrap
augroup END

""
"" vim-airline plugin

" airline does not appear unless this setting
set laststatus=2

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" Type :Edit to load multiple files at once
com! -complete=file -nargs=* Edit silent! exec "!vim --servername " . v:servername . " --remote-silent <args>"

""
"" CtrlP plugin

" Keymap to invoke CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" Root directory to start search for files
let g:ctrlp_working_path_mode = 'wa'

" Ignore pattern in search
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn|bzr)|\_site)$',
  \ 'file': '\v\.(exe|dll|class|png|jpg|jpeg|pdf)$',
\}
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

""
"" tagbar plugin

" Map F* to toggle tagbar window
nmap <F8> :TagbarToggle<CR>

""
"" UltiSnips plugin

" Tab to show possibilities, Ctrl+b & Ctrl+z to select forward & backward
let g:UltiSnipsSnippetsDir="~/.vim/bundle/vim-snippets/snippets"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

""
"" Syntastic plugin

" Use flake8 as syntax checker
let g:syntastic_python_checkers = ['flake8']"
let g:syntastic_check_on_open = 0

" Improve airline
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

""
"" Jedi plugin
let g:jedi#smart_auto_mappings = 0
let g:jedi#popup_on_dot = 0
let g:jedi#show_call_signatures = 0

""
"" vim-latex (latex-suite) plugin

" Set indentation to 2 spaces for tex files
autocmd FileType tex setlocal shiftwidth=2 tabstop=2

" Disable some key maps so we can type accented characters
" é
imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
" â
imap <buffer> <leader>bo <Plug>Tex_MathBF

" Reformat lines (getting the spacing correct) {{{
fun! TeX_fmt()
    if (getline(".") != "")
    let save_cursor = getpos(".")
        let op_wrapscan = &wrapscan
        set nowrapscan
	let par_begin = '^\(%D\)\=\s*\($\|\\begin\|\\end\|\\[\|\\]\|\\\(sub\)*section\>\|\\item\>\|\\NC\>\|\\blank\>\|\\noindent\>\)'
	let par_end   = '^\(%D\)\=\s*\($\|\\begin\|\\end\|\\[\|\\]\|\\place\|\\\(sub\)*section\>\|\\item\>\|\\NC\>\|\\blank\>\)'
    try
      exe '?'.par_begin.'?+'
    catch /E384/
      1
    endtry
        norm V
    try
      exe '/'.par_end.'/-'
    catch /E385/
      $
    endtry
    norm gq
        let &wrapscan = op_wrapscan
    call setpos('.', save_cursor)
    endif
endfun

nmap gqlp :call TeX_fmt()<CR>


""
"" localvimrc plugin

" Whitelist all local vimrc files for CubicWeb cubes
let g:localvimrc_persistent=1


""
"" tjp plugin

augroup filetypedetect
au BufNewFile,BufRead *.tjp,*.tji               setf tjp
augroup END

au! Syntax tjp          so ~/.vim/bundle/tjp/tjp.vim


""
"" HTML editing

" Set indentation for various filetypes
autocmd FileType html setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType php setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType css setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType vue setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType sql setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType json setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType twig setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType snakemake setlocal shiftwidth=4 tabstop=4 expandtab
